package tt.edu.sbcs.cis220;

/**
 * Represents a rectangle on the cartesian plain
 * given by (x,y), the top left hand corner of
 * the rectangle and width and height.
 */
public class Rectangle {
	private int x;
	private int y;
	private int width;
	private int height;
	
	/**
	 * Creates a Rectangle at position (0,0)
	 * and with width = 1, height = 1
	 */
	public Rectangle(){
		this(0, 0);
	}

	public Rectangle(int x, int y){
		this(x, y, 1, 1);
	}

	public Rectangle(int x, int y, int width, int height) throws IllegalArgumentException{
		if(width <= 0){
			throw new IllegalArgumentException("Width must be a positive integer");
		}
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;
	}

	/**
	 * Returns the width of the rectangle
	 * @return int value representing the width
	 */
	public int getWidth(){
		return this.width;
	}

	/**
	 * Sets a new width for this rectangle
	 * @param w int The new width of the rectangle
	 */
	public void setWidth(int width){
		this.width = width;
	}

	/**
	 * returns the height of this rectangle
	 */
	public int getHeight(){
		return height;
	}

 	/**
 	 * Sets a new height for this rectangle
 	 * @param h int the new height of the rectangle
 	 */
	public void setHeight(int height){
		this.height = height;
	} 

	/**P
	 * Returns the x coordinate of this rectangle
	 */
	public int getX(){
		return x;
	}

    /**
     * Sets a new x position for this rectangle
     *
     */
	public void setX(int x){
		this.x = x;
	}

	/**
	 * Returns the y coordinate of this rectangle
	 */
	public int getY(){
		return y;
	}

    /**
     * Sets a new y position for this rectangle
     *
     */
	public void setY(int y){
		this.y = y;
	}

	/**
	 * Returns a string representing a rectangle in the form
	 * "Rectangle:(x, y);width=this.width;height=this.height"
	 */
	public String toString(){

		return String.format(
			"Rectangle:(%d, %d);width=%d;height=%d",
			this.getX(), this.getY(), this.getWidth(), this.getHeight());

	}


}
