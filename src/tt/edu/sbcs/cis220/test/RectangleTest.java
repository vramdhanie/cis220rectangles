package tt.edu.sbcs.cis220.test;

import tt.edu.sbcs.cis220.Rectangle;

public class RectangleTest {
	public static void main(String args[]){
		Rectangle r = new Rectangle();
		Rectangle s = new Rectangle();
		System.out.println(r);

		System.out.println("Testing toString");
		String expected = "Rectangle:(0, 0);width=1;height=1";
		String actual = r.toString();
		System.out.println(expected.equals(actual)?"Passed":"Failed");


		//r.width = -3;
		r.setWidth(-3);
		System.out.println(r);

		r.setHeight(8);
		System.out.println(r);

		r.setX(5);
		System.out.println(r);

		r.setY(4);
		System.out.println(r);

		System.out.println("Testing Constructors");
		Rectangle t = new Rectangle(5, 7);
		System.out.println(t);
		System.out.printf("Constructor 2: %s\n", t.getX() == 5 && t.getY() == 7 && t.getWidth() == 1 && t.getHeight() == 1?"Passed":"Failed");

		Rectangle q = new Rectangle(6, 8, 9, 2);
		System.out.println(q);
		System.out.printf("Constructor 3: %s\n", q.getX() == 6 && q.getY() == 8 && q.getWidth() == 9 && q.getHeight() == 2?"Passed":"Failed");


		Rectangle u = new Rectangle(9, 9, -9, 7);










	}
}
