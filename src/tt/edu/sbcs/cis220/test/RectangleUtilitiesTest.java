package tt.edu.sbcs.cis220.test;

import tt.edu.sbcs.cis220.RectangleUtilities;
import tt.edu.sbcs.cis220.Rectangle;

public class RectangleUtilitiesTest {
	public static void main(String args[]){

		System.out.println(Math.round(Math.cos(Math.toRadians(90))));

		int[][] values = {
			{1,1,2,2,6,4},
			{5,1,2,2,6,4}, 
			{10,1,2,2,6,4},
			{5,2,2,2,6,4},
			{1,4,2,2,6,4},
			{2,4,2,2,6,4},
			{5,4,2,2,6,4},
			{8,4,2,2,6,4},
			{10,4,2,2,6,4},
			{5,6,2,2,6,4},
			{1,8,2,2,6,4},
			{5,8,2,2,6,4},
			{10,8,2,2,6,4}
		};
		String[] descriptions = {
			"Outside:top left octant", 
			"Outside:Top Centre octant", 
			"Outside:Top Right octant",
			"Inside:Top border",
			"Outside:Centre left octant",
			"Inside:left border",
			"Inside:Inside",
			"Inside:right border",
			"Outside:Centre right octant",
			"Inside:bottom border",
			"Outside:Bottom left octant",
			"Outside:Bottom Centre octant",
			"Outside:Bottom right octant",};
		boolean[] expected = {false, false, false, true, false,true, true, true, false, true, false, false, false};
		RectangleUtilities r = new RectangleUtilities();
		System.out.println("=====Testing containsPoint======");
		for(int i = 0; i < descriptions.length; i++){
			Rectangle re = new Rectangle();
			re.setX(values[i][2]);
			re.setY(values[i][3]);
			re.setWidth(values[i][4]);
			re.setHeight(values[i][5]);

			System.out.printf("%s: %s\n", r.containsPoint(values[i][0], values[i][1], re) == expected[i]?"Passed":"Failed", descriptions[i]);
		}

		//Here are some initial test cases for the intersects method
		//add more test cases.
		int[][] values2 = {
			{2,2,6,4,8,2,6,4},
			{2,2,6,4,3,3,4,4},
			{2,2,6,4,1,4,10,6}
		}; 
		String[] descriptions2 = {
			"Not intersecting",
			"One corner",
			"bottom half"
		};

		boolean[] expected2 = {false, true, true};

		System.out.println("=====Testing intersects======");
		for(int i = 0; i < descriptions2.length; i++){
			System.out.println(String.format("%s: %s", r.intersects(values2[i][0], values2[i][1], values2[i][2], values2[i][3], values2[i][4], values2[i][5], values2[i][6], values2[i][7]) == expected2[i]?"Passed":"Failed", descriptions2[i]));
		}

	}
}
