#CIS 220 Graphical Object Oriented Programming
This project is for the CIS220 class from the University of London taught at [SBCS](http://www.sbcs.edu.tt).

##Rectangle Project
Used to illustrate some basic principles:

 - Class design
 - Conventions
 - Documentation
 - Source control

 